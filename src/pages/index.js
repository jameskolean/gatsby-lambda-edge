import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Protecting Static Content</h1>
    <a href="/public-page/">Public Page</a>
    <br />
    <a href="/protected-page/">Protected Page</a>
  </Layout>
)

export default IndexPage
