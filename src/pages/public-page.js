import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PublicPage = () => (
  <Layout>
    <SEO title="Public Page" />
    <h1>Hi from the public page</h1>
    <p>Welcome to public page</p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default PublicPage
